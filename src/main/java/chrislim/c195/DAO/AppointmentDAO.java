package chrislim.c195.DAO;

import chrislim.c195.controller.LoginController;
import chrislim.c195.helper.JDBC;
import chrislim.c195.model.AppointmentModel;
import chrislim.c195.model.ReportModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

/**
 * A class that handles Appointment query in the database
 * @author Christopher Lim
 */
public class AppointmentDAO {
    private static final DateTimeFormatter datetimeDTF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final ZoneId localZoneID = ZoneId.systemDefault();
    private static final ZoneId utcZoneID = ZoneId.of("UTC");

    /**
     * This method query if there is any upcoming appointments
     * @param userId the user id
     * @return the appointment model if the query is found, if not return null
     * @throws SQLException if there is an error
     */
    public static AppointmentModel getUpcomingAppointment(int userId) throws SQLException {
        String SQL = "SELECT * FROM Appointments WHERE user_id = ? AND Start BETWEEN ? AND ?";

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime in15Minutes = now.plusMinutes(15);

        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL)) {
            ps.setInt(1, userId);
            ps.setTimestamp(2, Timestamp.valueOf(now));
            ps.setTimestamp(3, Timestamp.valueOf(in15Minutes));

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int appointmentId = rs.getInt("Appointment_ID");
                    int customerId = rs.getInt("Customer_ID");
                    int contactId = rs.getInt("Contact_ID");
                    String title = rs.getString("Title");
                    String description = rs.getString("Description");
                    String location = rs.getString("Location");
                    String type = rs.getString("Type");
                    LocalDateTime startDateTime = rs.getTimestamp("Start").toLocalDateTime();
                    LocalDateTime endDateTime = rs.getTimestamp("End").toLocalDateTime();
                    LocalDate startDate = startDateTime.toLocalDate();
                    LocalDate endDate = endDateTime.toLocalDate();
                    LocalTime startTime = startDateTime.toLocalTime();
                    LocalTime endTime = endDateTime.toLocalTime();

                    return new AppointmentModel(appointmentId, customerId, userId, contactId, title, description,
                            location, type, startDateTime, endDateTime, startDate, endDate, startTime, endTime);
                }
            }
        }
        return null;
    }

    /**
     * This method is for creating appointment model from the result set
     * @param rs this is result set
     * @return the appointment model
     * @throws SQLException if there is an error
     */
    private static AppointmentModel createAppointmentFromResultSet(ResultSet rs) throws SQLException {
        int appointmentID = rs.getInt(1);
        String appointmentTitle = rs.getString(2);
        String appointmentDescription = rs.getString(3);
        String appointmentLocation = rs.getString(4);
        String appointmentType = rs.getString(5);
        int customerID = rs.getInt(12);
        int userID = rs.getInt(13);
        int contactID = rs.getInt(14);
        LocalDateTime utcStartDT = LocalDateTime.parse(rs.getString(6), datetimeDTF);
        LocalDateTime utcEndDT = LocalDateTime.parse(rs.getString(7), datetimeDTF);
        LocalDateTime localStartDT = utcStartDT.atZone(utcZoneID).withZoneSameInstant(localZoneID).toLocalDateTime();
        LocalDateTime localEndDT = utcEndDT.atZone(utcZoneID).withZoneSameInstant(localZoneID).toLocalDateTime();
        LocalDate localStartDate = localStartDT.toLocalDate();
        LocalDate localEndDate = localEndDT.toLocalDate();
        LocalTime localStartTime = localStartDT.toLocalTime();
        LocalTime localEndTime = localEndDT.toLocalTime();
        return new AppointmentModel(appointmentID, customerID, userID, contactID, appointmentTitle, appointmentDescription, appointmentLocation, appointmentType, localStartDT, localEndDT, localStartDate, localEndDate, localStartTime, localEndTime);
    }

    /**
     * This method runs a for loop that checks for the appointment query
     * and then return the created appointment from the result set
     * @param query the query
     * @param parameters the parameters
     * @return all the appointments
     */
    private static ObservableList<AppointmentModel> getAppointmentsWithQuery(String query, String... parameters) {
        ObservableList<AppointmentModel> allAppointments = FXCollections.observableArrayList();
        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(query)) {
            for (int i = 0; i < parameters.length; i++) {
                ps.setString(i + 1, parameters[i]);
            }
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    AppointmentModel newAppointment = createAppointmentFromResultSet(rs);
                    allAppointments.add(newAppointment);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allAppointments;
    }

    /**
     * This method run the query for all appointments
     * @return the getAppointmentsWithQuery method
     */
    public static ObservableList<AppointmentModel> getAllAppointments() {
        return getAppointmentsWithQuery("SELECT * FROM APPOINTMENTS");
    }

    /**
     * This method runs a query to find all the appointments for the current week
     * @return the getAppointmentsWithQuery method
     */
    public static ObservableList<AppointmentModel> getAllAppointmentsThisWeek() {
        LocalDate today = LocalDate.now();
        LocalDate startOfWeek = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.SUNDAY));
        LocalDate endOfWeek = today.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));
        String selectAppointmentsThisWeekQuery = "SELECT * FROM APPOINTMENTS WHERE DATE(start) BETWEEN ? AND ? ORDER BY CAST(APPOINTMENT_ID AS unsigned)";
        return getAppointmentsWithQuery(selectAppointmentsThisWeekQuery, startOfWeek.toString(), endOfWeek.toString());
    }

    /**
     * This method runs a query to find all the appointments for the current month
     * @return the getAppointmentsWithQuery method
     */
    public static ObservableList<AppointmentModel> getAllAppointmentsThisMonth() {
        YearMonth currentYearMonth = YearMonth.now();
        LocalDate startDate = currentYearMonth.atDay(1);
        LocalDate endDate = currentYearMonth.atEndOfMonth();
        String selectAppointmentsThisMonthQuery = "SELECT * FROM APPOINTMENTS WHERE DATE(start) BETWEEN ? AND ? ORDER BY CAST(APPOINTMENT_ID AS unsigned)";
        return getAppointmentsWithQuery(selectAppointmentsThisMonthQuery, startDate.toString(), endDate.toString());
    }

    /**
     * This method runs a query to delete an appointment from the database
     * @param selectedAppointmentId the selected appointment id
     * @return rowsAffected
     * @throws SQLException if there is an error
     */
    public static boolean deleteAppointmentFromDB(int selectedAppointmentId) throws SQLException {
        String SQL = "DELETE FROM APPOINTMENTS WHERE Appointment_ID = ?";
        PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL);
        ps.setInt(1, selectedAppointmentId);
        int rowsAffected = ps.executeUpdate();
        return rowsAffected > 0;
    }

    /**
     * This method adds the appointment to the database
     * @param appointment the appointment
     * @return the rowsAffected
     * @throws SQLException if there is an error
     */
    public static boolean addAppointmentToDB(AppointmentModel appointment) throws SQLException {
        System.out.println(appointment);
        String SQL = "INSERT INTO appointments (Title, Description, Location, Type, Start, End, Contact_ID, Customer_ID, User_ID, Create_Date, Created_By, Last_Update, Last_Updated_By) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        int rowsAffected = 0;
        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL)) {
            ps.setString(1, appointment.getTitle());
            ps.setString(2, appointment.getDescription());
            ps.setString(3, appointment.getLocation());
            ps.setString(4, appointment.getType());
            ps.setTimestamp(5, Timestamp.valueOf(appointment.getStartDateTime()));
            ps.setTimestamp(6, Timestamp.valueOf(appointment.getEndDateTime()));
            ps.setInt(7, appointment.getContactId());
            ps.setInt(8, appointment.getCustomerId());
            ps.setInt(9, appointment.getUserId());
            ps.setTimestamp(10, Timestamp.valueOf(LocalDateTime.now()));
            ps.setString(11, LoginController.getCurrentUser().getUsername());
            ps.setTimestamp(12, Timestamp.valueOf(LocalDateTime.now()));
            ps.setString(13, LoginController.getCurrentUser().getUsername());
            rowsAffected = ps.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return rowsAffected > 0;
    }

    /**
     * This method makes a query to check of there is an overlap between the appointments
     * @param newAppointment the new appointment
     * @return true or false
     * @throws SQLException if there is an error
     */
    public static boolean checkForOverlap(AppointmentModel newAppointment) throws SQLException {
        String SQL = "SELECT * FROM appointments WHERE Appointment_ID != ? AND Customer_ID = ? AND ((Start < ? AND End > ?) OR (Start < ? AND End > ?) OR (Start >= ? AND End <= ?))";
        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL)) {
            ps.setInt(1, newAppointment.getAppointmentId());
            ps.setInt(2, newAppointment.getCustomerId());
            ps.setTimestamp(3, Timestamp.valueOf(newAppointment.getStartDateTime()));
            ps.setTimestamp(4, Timestamp.valueOf(newAppointment.getStartDateTime()));
            ps.setTimestamp(5, Timestamp.valueOf(newAppointment.getEndDateTime()));
            ps.setTimestamp(6, Timestamp.valueOf(newAppointment.getEndDateTime()));
            ps.setTimestamp(7, Timestamp.valueOf(newAppointment.getStartDateTime()));
            ps.setTimestamp(8, Timestamp.valueOf(newAppointment.getEndDateTime()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                // If the result set is not empty, it means there is an overlapping appointment
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return false;
    }

    /**
     * This method makes a query to check not used appointment id
     * @return the maxAppointmentId
     * @throws SQLException if there is an error
     */
    public static int getUnusedAppointmentId() throws SQLException {
        String SQL = "SELECT MAX(Appointment_ID) AS MaxAppointmentID FROM appointments";
        int maxAppointmentId = 0;

        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {
            if(rs.next()) {
                maxAppointmentId = rs.getInt("MaxAppointmentID");
            }
        } catch(SQLException e) {
            e.printStackTrace();
            throw e;
        }

        return maxAppointmentId + 1;
    }

    /**
     * The method query the current appointment and then updates it
     * @param appointment the appointment
     * @return the rowsUpdated
     * @throws SQLException if there is an error
     */
    public static boolean updateAppointmentInDB(AppointmentModel appointment) throws SQLException {
        String SQL = "UPDATE appointments SET Title = ?, Description = ?, Location = ?, Type = ?, Start = ?, End = ?, Contact_ID = ?, Customer_ID = ?, User_ID = ?, Last_Update = ?, Last_Updated_By = ? WHERE Appointment_ID = ?";

        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL)) {
            ps.setString(1, appointment.getTitle());
            ps.setString(2, appointment.getDescription());
            ps.setString(3, appointment.getLocation());
            ps.setString(4, appointment.getType());
            ps.setTimestamp(5, Timestamp.valueOf(appointment.getStartDateTime()));
            ps.setTimestamp(6, Timestamp.valueOf(appointment.getEndDateTime()));
            ps.setInt(7, appointment.getContactId());
            ps.setInt(8, appointment.getCustomerId());
            ps.setInt(9, appointment.getUserId());
            ps.setTimestamp(10, Timestamp.valueOf(LocalDateTime.now()));
            ps.setString(11, LoginController.getCurrentUser().getUsername());
            ps.setInt(12, appointment.getAppointmentId());

            int rowsUpdated = ps.executeUpdate();

            return rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * This method runs a query to get all Appointments by a Contact Id
     * @param selectedContactId the selected contact id
     * @return the getAppointmentsWithQuery method
     */
    public static ObservableList<AppointmentModel> getAppointmentsByContact(int selectedContactId) {
        String selectAppointmentsByContactQuery = "SELECT * FROM APPOINTMENTS WHERE Contact_ID = ?";
        return getAppointmentsWithQuery(selectAppointmentsByContactQuery, Integer.toString(selectedContactId));
    }
}
