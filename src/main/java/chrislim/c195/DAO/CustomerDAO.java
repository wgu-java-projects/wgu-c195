package chrislim.c195.DAO;

import chrislim.c195.controller.LoginController;
import chrislim.c195.helper.JDBC;
import chrislim.c195.model.CustomerModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;


/**
 * A class that handles Customer query in the database
 * @author Christopher Lim
 */
public class CustomerDAO {
    /**
     * This method query for the unused maxed id of the customer
     * @return the default id
     * @throws SQLException if there is an error
     */
    public static int getUnusedCustomerId() throws SQLException {
        String SQL = "SELECT MAX(Customer_ID) as MaxId FROM CUSTOMERS";

        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {

            if (rs.next()) {
                return rs.getInt("MaxId") + 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }

        // Default ID if no entries in the table
        return 1;
    }

    /**
     * This method runs a query to add a customer to the database
     * @param customer the customer
     * @return the affected rows
     * @throws SQLException if there is an error
     */
    public static boolean addCustomerToDB(CustomerModel customer) throws SQLException {
        String SQL = "INSERT INTO customers (Customer_ID, Customer_Name, Address, Postal_Code, Phone, Create_Date, Created_By, Last_Update, Last_Updated_By, Division_ID) VALUES (?,?,?,?,?,?,?,?,?,?)";
        int rowsAffected = 0;
        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL)) {
            ps.setInt(1, customer.getCustomerId());
            ps.setString(2, customer.getCustomerName());
            ps.setString(3, customer.getAddress());
            ps.setString(4, customer.getPostalCode());
            ps.setString(5, customer.getPhone());
            ps.setTimestamp(6, Timestamp.valueOf(LocalDateTime.now()));
            ps.setString(7, LoginController.getCurrentUser().getUsername());
            ps.setTimestamp(8, Timestamp.valueOf(LocalDateTime.now()));
            ps.setString(9, LoginController.getCurrentUser().getUsername());
            ps.setInt(10, customer.getDivisionId());

            rowsAffected = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return rowsAffected > 0;
    }

    /**
     * This method query for the customer already in the database and then updates it
     * @param customer the customer
     * @return the rowsAffected
     * @throws SQLException if there is an error
     */
    public static boolean updateCustomerInDB(CustomerModel customer) throws SQLException {
        String SQL = "UPDATE customers SET Customer_Name = ?, Address = ?, Postal_Code = ?, Phone = ?, Last_Update = ?, Last_Updated_By = ?, Division_ID = ? WHERE Customer_ID = ?";
        int rowsAffected = 0;
        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL)) {
            ps.setString(1, customer.getCustomerName());
            ps.setString(2, customer.getAddress());
            ps.setString(3, customer.getPostalCode());
            ps.setString(4, customer.getPhone());
            ps.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
            ps.setString(6, LoginController.getCurrentUser().getUsername());
            ps.setInt(7, customer.getDivisionId());
            ps.setInt(8, customer.getCustomerId());
            rowsAffected = ps.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return rowsAffected > 0;
    }

    /**
     * This method query all the customers
     * @return the getCustomersWithQuery method
     */
    public static ObservableList<CustomerModel> getAllCustomers() {
        String SQL = "SELECT customers.CUSTOMER_ID, customers.CUSTOMER_NAME, customers.ADDRESS, customers.POSTAL_CODE, customers.PHONE, " +
                "first_level_divisions.DIVISION_ID, first_level_divisions.DIVISION, " +
                "countries.COUNTRY_ID, countries.COUNTRY " +
                "FROM customers " +
                "JOIN first_level_divisions ON customers.DIVISION_ID = first_level_divisions.DIVISION_ID " +
                "JOIN countries ON first_level_divisions.COUNTRY_ID = countries.COUNTRY_ID";
        return getCustomersWithQuery(SQL);
    }

    /**
     * This method query the customer by country
     * @param selectedCountryId the selected country id
     * @return the getCustomersWithQuery method
     */
    public static ObservableList<CustomerModel> getCustomersByCountry(int selectedCountryId) {
        String SQL = "SELECT customers.CUSTOMER_ID, customers.CUSTOMER_NAME, customers.ADDRESS, customers.POSTAL_CODE, customers.PHONE, " +
                "first_level_divisions.DIVISION_ID, first_level_divisions.DIVISION, " +
                "countries.COUNTRY_ID, countries.COUNTRY " +
                "FROM customers " +
                "JOIN first_level_divisions ON customers.DIVISION_ID = first_level_divisions.DIVISION_ID " +
                "JOIN countries ON first_level_divisions.COUNTRY_ID = countries.COUNTRY_ID " +
                "WHERE countries.COUNTRY_ID = ?";
        return getCustomersWithQuery(SQL, selectedCountryId);
    }

    /**
     * This method query a customer from the database
     * @param query the query
     * @param parameters the parameters
     * @return all customers
     */
    private static ObservableList<CustomerModel> getCustomersWithQuery(String query, Object... parameters) {
        ObservableList<CustomerModel> allCustomers = FXCollections.observableArrayList();
        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(query)) {
            for (int i = 0; i < parameters.length; i++) {
                ps.setObject(i + 1, parameters[i]);
            }
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CustomerModel newCustomer = createCustomerFromResultSet(rs);
                    allCustomers.add(newCustomer);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allCustomers;
    }


    /**
     * The method that creates the customer from result set
     * @param rs the rs
     * @return the Customer model
     * @throws SQLException if there is an error
     */
    private static CustomerModel createCustomerFromResultSet(ResultSet rs) throws SQLException {
        int customerId = rs.getInt("CUSTOMER_ID");
        String customerName = rs.getString("CUSTOMER_NAME");
        String address = rs.getString("ADDRESS");
        String postalCode = rs.getString("POSTAL_CODE");
        String phone = rs.getString("PHONE");
        int divisionId = rs.getInt("DIVISION_ID");
        String divisionName = rs.getString("DIVISION");
        int countryId = rs.getInt("COUNTRY_ID");
        String countryName = rs.getString("COUNTRY");
        return new CustomerModel(customerId, divisionId, countryId, customerName, address, postalCode, phone, countryName, divisionName);
    }


    /**
     * This method makes a query and delete a customer to the database
     * @param selectedCustomerId the selected customer id
     * @return rowsAffected
     * @throws SQLException if there is an error
     */
    public static boolean deleteCustomerFromDB(int selectedCustomerId) throws SQLException {
        String SQL = "DELETE FROM CUSTOMERS WHERE Customer_ID = ?";
        PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL);
        ps.setInt(1, selectedCustomerId);
        int rowsAffected = ps.executeUpdate();
        return rowsAffected > 0;
    }
}
