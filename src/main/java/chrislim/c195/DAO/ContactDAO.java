package chrislim.c195.DAO;

import chrislim.c195.helper.JDBC;
import chrislim.c195.model.ContactModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class that handles Contact query in the database
 * @author Christopher Lim
 */
public class ContactDAO {
    /**
     * This method runs a query to get all contacts fom the database
     * @return all contacts
     * @throws SQLException if there is an error
     */
    public static ObservableList<ContactModel> getAllContacts() throws SQLException {
        ObservableList<ContactModel> allContacts = FXCollections.observableArrayList();
        String SQL = "SELECT * FROM CONTACTS";

        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                String contactName = rs.getString("CONTACT_NAME");
                int contactId = rs.getInt("CONTACT_ID");
                ContactModel contact = new ContactModel(contactId, contactName);
                allContacts.add(contact);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }

        return allContacts;
    }
}
