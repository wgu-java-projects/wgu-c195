package chrislim.c195.DAO;

import chrislim.c195.helper.JDBC;
import chrislim.c195.model.ReportModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class that handles Reports query in the database
 * @author Christopher Lim
 */
public class ReportDAO {
    /**
     * This method query all the reports that starts with month, type and count
     * @return reports
     * @throws SQLException if there is an error
     */
    public static ObservableList<ReportModel> getAllReports() throws SQLException {
        ObservableList<ReportModel> reports = FXCollections.observableArrayList();

        String SQL = "SELECT MONTHNAME(Start) AS Month, Type, COUNT(*) AS Count " +
                "FROM Appointments " +
                "GROUP BY Month, Type";

        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String month = rs.getString("month");
                    String type = rs.getString("type");
                    int count = rs.getInt("count");
                    ReportModel report = new ReportModel(month, type, count);
                    reports.add(report);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reports;
    }
}
