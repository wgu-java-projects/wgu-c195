package chrislim.c195.DAO;

import chrislim.c195.helper.JDBC;
import chrislim.c195.model.CountryModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class that handles Country query in the database
 * @author Christopher Lim
 */
public class CountryDAO {
    /**
     * This method query for all the countries
     * @return allCountries
     * @throws SQLException if there is an error
     */
    public static ObservableList<CountryModel> getAllCountries() throws SQLException {
        ObservableList<CountryModel> allCountries = FXCollections.observableArrayList();
        String SQL = "SELECT COUNTRY, COUNTRY_ID FROM COUNTRIES";

        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                String countryName = rs.getString("COUNTRY");
                int countryId = rs.getInt("COUNTRY_ID");
                CountryModel country = new CountryModel(countryId, countryName);
                allCountries.add(country);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }

        return allCountries;
    }
}
