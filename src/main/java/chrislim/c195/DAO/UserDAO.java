package chrislim.c195.DAO;

import chrislim.c195.helper.JDBC;
import chrislim.c195.model.UserModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class that handles User query in the database
 * @author Christopher Lim
 */
public class UserDAO {
    /**
     * This method runs a query to get all users from the database
     * @return all users
     * @throws SQLException if there is an error
     */
    public static ObservableList<UserModel> getAllUsers() throws SQLException {
        ObservableList<UserModel> allUsers = FXCollections.observableArrayList();
        String SQL = "SELECT * FROM USERS";
        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                int userId = rs.getInt("USER_ID");
                String userName = rs.getString("USER_NAME");
                String password = rs.getString("PASSWORD");
                UserModel user = new UserModel(userId, userName, password);
                allUsers.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return allUsers;
    }
}
