package chrislim.c195.DAO;

import chrislim.c195.helper.JDBC;
import chrislim.c195.model.DivisionModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class that handles Division query in the database
 * @author Christopher Lim
 */
public class DivisionDAO {
    /**
     * This method query all the division by country id
     * @param selectedCountryId the selected country id
     * @return allDivisionsByCountryId
     * @throws SQLException if there is an error
     */
    public static ObservableList<DivisionModel> getAllDivisionsByCountryId(int selectedCountryId) throws SQLException {
        ObservableList<DivisionModel> allDivisionsByCountryId = FXCollections.observableArrayList();
        String SQL = "SELECT * FROM FIRST_LEVEL_DIVISIONS WHERE COUNTRY_ID = ?";

        try (PreparedStatement ps = JDBC.getConnection().prepareStatement(SQL)) {
            ps.setInt(1, selectedCountryId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    int divisionId = rs.getInt("Division_ID");
                    String divisionName = rs.getString("Division");
                    DivisionModel division = new DivisionModel(divisionId, divisionName);
                    allDivisionsByCountryId.add(division);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }

        return allDivisionsByCountryId;
    }
}
