package chrislim.c195.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * A Class to create Appointment object in the table
 * @author Christopher Lim
 */
public class AppointmentModel {
    private int appointmentId;
    private int customerId;
    private int userId;
    private int contactId;
    private String title;
    private String description;
    private String location;
    private String type;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalTime startTime;
    private LocalTime endTime;

    /**
     * Initializes a new instance for the Appointment class
     * @param appointmentId the appointment id
     * @param customerId the customer id
     * @param userId the user id
     * @param contactId the contact id
     * @param title the title of the appointment
     * @param description the description of the appointment
     * @param location the location of the appointment
     * @param type the type of the appointment
     * @param startDateTime the start date and time of the appointment
     * @param endDateTime the end date and time of the appointment
     * @param startDate the start date of the appointment
     * @param endDate the end date of the appointment
     * @param startTime the start time of the appointment
     * @param endTime the end time of the appointment
     */
    public AppointmentModel(int appointmentId, int customerId, int userId, int contactId, String title,
                            String description, String location, String type, LocalDateTime startDateTime,
                            LocalDateTime endDateTime, LocalDate startDate, LocalDate endDate,
                            LocalTime startTime, LocalTime endTime) {
        this.appointmentId = appointmentId;
        this.customerId = customerId;
        this.userId = userId;
        this.contactId = contactId;
        this.title = title;
        this.description = description;
        this.location = location;
        this.type = type;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     * Get the appointment id
     * @return the appointment id
     */
    public int getAppointmentId() {
        return appointmentId;
    }

    /**
     * Set the appointment id
     * @param appointmentId the appointment id
     */
    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }

    /**
     * Get the customer id
     * @return the customer id
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Set the customer id
     * @param customerId the customer id
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     * Get the user id
     * @return the user id
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Set the user id
     * @param userId the user id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Get the contact id
     * @return the contact id
     */
    public int getContactId() {
        return contactId;
    }

    /**
     * Set the contact id
     * @param contactId the contact id
     */
    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    /**
     * Get the title
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the title
     * @param title the tile
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get the description
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the location
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Set the location
     * @param location the location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Get the type
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Set the type
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get the start date time
     * @return the start date time
     */
    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    /**
     * Set the start date time
     * @param startDateTime the start date time
     */
    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    /**
     * Get the end date time
     * @return the end date time
     */
    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    /**
     * Set the end date time
     * @param endDateTime the end date time
     */
    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    /**
     * Get the start date
     * @return the start date
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * Set the start date
     * @param startDate the start date
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * Get the end date
     * @return the end date
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * Set the end date
     * @param endDate the end date
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * Get the start time
     * @return the start time
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * Set the start time
     * @param startTime the start time
     */
    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     * Get the end time
     * @return the end time
     */
    public LocalTime getEndTime() {
        return endTime;
    }

    /**
     * Set the end time
     * @param endTime the end time
     */
    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    /**
     * Provides default syntax for appointment information.
     * @return string of customer info
     */
    @Override
    public String toString() {
        return ("Appt: [" + appointmentId + "] | Customer: [" + customerId + "] " +
                "| Contact: [" + contactId + "] | Type: " + type + "| Start: " + startDateTime
                + " | End: " + endDateTime );
    }
}
