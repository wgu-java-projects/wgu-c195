package chrislim.c195.model;

/**
 * A Class to create Report object in the table
 * @author Christopher Lim
 */
public class ReportModel {
    private String month;
    private String type;
    private int count;

    /**
     * Initializes a new instance for the Report class
     * @param month the month of the appointment
     * @param type the type of the appointment
     * @param count the count of the appointment
     */
    public ReportModel(String month, String type, int count) {
        this.month = month;
        this.type = type;
        this.count = count;
    }

    /**
     * Gets the month
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * Sets the month
     * @param month the month
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * Gets the type
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the count
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets the count
     * @param count the count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Returns a string representation of the report, including its month, type, and count.
     * @return string representation
     */
    @Override
    public String toString() {
        return "Report: " + month + " " + type + " " + count;
    }
}
