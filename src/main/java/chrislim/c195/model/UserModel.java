package chrislim.c195.model;

/**
 * A Class to create User object in the table
 * @author Christopher Lim
 */
public class UserModel {
    private int userID;
    private String username;
    private String password;

    /**
     * A Class to create User object in the table
     * @param userID the user id
     * @param username the user name
     * @param password the password
     */
    public UserModel(int userID, String username, String password) {
        this.userID = userID;
        this.username = username;
        this.password = password;
    }

    /**
     * Gets the user id
     * @return the user id
     */
    public int getUserID() {
        return userID;
    }

    /**
     * Sets the user id
     * @param userID the user id
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }

    /**
     * Gets the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the password
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Provides default syntax for database user information.
     * @return string concatenating userID and username
     * */
    @Override
    public String toString() {
        return ("[" + userID + "] " + username);
    }
}
