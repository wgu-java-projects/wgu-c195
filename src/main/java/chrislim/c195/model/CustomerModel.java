package chrislim.c195.model;

/**
 * A Class to create Customer object in the table
 * @author Christopher Lim
 */
public class CustomerModel {
    private int customerId;
    private int divisionId;
    private int countryId;
    private String customerName;
    private String address;
    private String postalCode;
    private String phone;
    private String countryName;
    private String divisionName;

    /**
     * Initializes a new instance for the Customer class
     * @param customerId the customer id of the customer
     * @param divisionId   the division id
     * @param countryId    the country id
     * @param customerName the customer name
     * @param address      the address of the customer
     * @param postalCode   the postal code of the customer
     * @param phone        the phone number of the customer
     * @param countryName  the country of the customer
     * @param divisionName the division of the customer
     */
    public CustomerModel(int customerId, int divisionId, int countryId, String customerName,
                         String address, String postalCode, String phone, String countryName, String divisionName) {
        this.customerId = customerId;
        this.divisionId = divisionId;
        this.countryId = countryId;
        this.customerName = customerName;
        this.address = address;
        this.postalCode = postalCode;
        this.phone = phone;
        this.countryName = countryName;
        this.divisionName = divisionName;
    }

    /**
     * Gets the customer id
     * @return the customer id
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customer id
     * @param customerId the customer id
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the division id
     * @return the division id
     */
    public int getDivisionId() {
        return divisionId;
    }

    /**
     * Sets the division id
     * @param divisionId the id
     */
    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    /**
     * Gets the country id
     * @return the country id
     */
    public int getCountryId() {
        return countryId;
    }

    /**
     * Sets the country id
     * @param countryId the country id
     */
    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    /**
     * Gets the customer name
     * @return the customer name
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Sets the customer name
     * @param customerName the customer name
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * Gets the address
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets the postal code
     * @return the postal code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the postal code
     * @param postalCode the postal code
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Gets the phone
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the phone
     * @param phone the phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Gets the country name
     * @return the country name
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Sets the country name
     * @param countryName the country name
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * Gets the division name
     * @return the division name
     */
    public String getDivisionName() {
        return divisionName;
    }

    /**
     * Sets the division name
     * @param divisionName the division name
     */
    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    /**
     * Provides default syntax for customer information.
     * @return string of customer info
     * */
    @Override
    public String toString() {
        return ("[" + Integer.toString(customerId) + "] " + customerName);
    }

}
