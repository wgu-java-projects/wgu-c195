package chrislim.c195.model;

/**
 * A Class to create Country object in the table
 * @author Christopher Lim
 */
public class CountryModel {
    private int countryId;
    private String countryName;

    /**
     * Initializes a new instance for the Country class
     * @param countryId the country id
     * @param countryName the country name
     */
    public CountryModel(int countryId, String countryName) {
        this.countryId = countryId;
        this.countryName = countryName;
    }

    /**
     * Gets the country id
     * @return the country id
     */
    public int getCountryId() {
        return countryId;
    }

    /**
     * Sets the country id
     * @param countryId the country id
     */
    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    /**
     * Gets the country name
     * @return the country name
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Sets the country name
     * @param countryName the country name
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * Returns the name of the Country.
     * @return the string for country
     */
    @Override
    public String toString() {
        return countryName;
    }
}
