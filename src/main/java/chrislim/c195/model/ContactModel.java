package chrislim.c195.model;

/**
 * A Class to create Contact object in the table
 * @author Christopher Lim
 */
public class ContactModel {
    private int contactId;
    private String contactName;

    /**
     * Initializes a new instance for the Contact class
     * @param contactId the contact id
     * @param contactName the contact name
     */
    public ContactModel(int contactId, String contactName) {
        this.contactId = contactId;
        this.contactName = contactName;
    }

    /**
     * Gets the contact id
     * @return the contact id
     */
    public int getContactId() {
        return contactId;
    }

    /**
     * Sets the contact id
     * @param contactId the contact id
     */
    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    /**
     * Gets the contact name
     * @return the contact name
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Sets the contact name
     * @param contactName the contact name
     */
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * Returns a string representation of the Contact, including its ID and name.
     * @return string rep
     */
    @Override
    public String toString() {
        return "[" + contactId + "] " + contactName;
    }
}
