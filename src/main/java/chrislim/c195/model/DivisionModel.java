package chrislim.c195.model;

/**
 * A Class to create Division object in the table
 * @author Christopher Lim
 */
public class DivisionModel {
    private int divisionId;
    private String divisionName;

    /**
     * Initializes a new instance for the Division class
     * @param divisionId the division id
     * @param divisionName the division name
     */
    public DivisionModel(int divisionId, String divisionName) {
        this.divisionId = divisionId;
        this.divisionName = divisionName;
    }

    /**
     * Gets the division id
     * @return the division id
     */
    public int getDivisionId() {
        return divisionId;
    }

    /**
     * Sets the division id
     * @param divisionId the division id
     */
    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    /**
     * Gets the division name
     * @return the division name
     */
    public String getDivisionName() {
        return divisionName;
    }

    /**
     * Sets the division name
     * @param divisionName the division name
     */
    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    /**
     * Returns a string representation of the division, including its name.
     * @return the string for the division name
     */
    @Override
    public String toString() {
        return divisionName;
    }
}
