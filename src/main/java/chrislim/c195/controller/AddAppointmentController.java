package chrislim.c195.controller;

import chrislim.c195.DAO.AppointmentDAO;
import chrislim.c195.DAO.ContactDAO;

import chrislim.c195.DAO.CustomerDAO;
import chrislim.c195.DAO.UserDAO;
import chrislim.c195.helper.Utilities;

import chrislim.c195.model.AppointmentModel;
import chrislim.c195.model.ContactModel;
import chrislim.c195.model.UserModel;
import chrislim.c195.model.CustomerModel;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Stream;

/**
 * Controller for Add Appointment View
 * @author Christopher Lim
 */
public class AddAppointmentController implements Initializable {
    String businessHours[] =
            { "8:00", "8:30", "9:00", "9:30", "10:00", "10:30", "11:00", "11:30", "13:00", "13:30",
                    "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "18:00", "18:30",
                    "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00"};

    @FXML private TextField appointmentId;
    @FXML private TextField title;
    @FXML private TextField description;
    @FXML private TextField location;
    @FXML private TextField type;
    @FXML private ComboBox<ContactModel> contact;
    @FXML private ComboBox<CustomerModel> customer;
    @FXML private ComboBox<UserModel> user;
    @FXML private DatePicker startDate;
    @FXML private DatePicker endDate;
    @FXML private ComboBox<String> startTime;
    @FXML private ComboBox<String> endTime;
    @FXML private Button addButton;
    @FXML private Button cancelButton;

    /**
     * Adds appointment to the list
     * @param actionEvent ActionEvent object
     * @throws IOException if there is an error loading
     */
    public void addAppointment(ActionEvent actionEvent) throws IOException {
        if (isAnyFieldEmpty()) {
            Utilities.customError("All fields must be filled!","Please check to see if there is any missing information.");
            return;
        }

        try {
            AppointmentModel newAppointment = createAppointmentFromFormInputs();
            if (newAppointment == null) {
                return;
            }

            if (isAppointmentWithinBusinessHours(newAppointment)) {
                if (!AppointmentDAO.checkForOverlap(newAppointment)) {
                    if (addAppointmentToDBAndNavigateBack(newAppointment, actionEvent)) {
                        return;
                    }
                    Utilities.customError("Failed to add appointment!","Check your parameters or debug.");
                } else {
                    Utilities.customError("Overlapping appointments for selected customer!","Please change the time.");
                }
            } else {
                Utilities.customError("Not within business hours!","Please change the time.");
            }
        } catch (SQLException | DateTimeParseException e) {
            e.printStackTrace();
            Utilities.customError("Incorrect time format!","Please change the time.");
        }
    }

    /**
     * This method checks if any of the required text fields is empty
     * LAMBDA EXPRESSION: Gets a String from the Stream and tests for null or empty.
     * The predicate "(text -> text == null || text.isEmpty())"
     * returns true if the String is null or empty and false otherwise.
     * @return true if any required text box is empty, false otherwise
     */
    private boolean isAnyFieldEmpty() {
        return Stream.of(title.getText(), description.getText(), location.getText(), type.getText())
                .anyMatch(text -> text == null || text.isEmpty());
    }

    /**
     * This method creates a new appointment
     * @return a new Appointment object
     * @throws DateTimeParseException if the start time or end time is not a valid time
     * @throws SQLException if there is a problem accessing the database
     */
    private AppointmentModel createAppointmentFromFormInputs() throws DateTimeParseException, SQLException {
        int contactSelected = contact.getSelectionModel().getSelectedItem().getContactId();
        int customerSelected = customer.getSelectionModel().getSelectedItem().getCustomerId();
        int userSelected = user.getSelectionModel().getSelectedItem().getUserID();
        LocalDate startDateValue = startDate.getValue();
        LocalDate endDateValue = endDate.getValue();
        LocalTime startTimeValue = LocalTime.parse(startTime.getValue());
        LocalTime endTimeValue = LocalTime.parse(endTime.getValue());

        if (startDateValue.isAfter(endDateValue)) {
            Utilities.customError("Start date and end date are not chronologically plausible!","Please change the date.");
            return null;
        }

        if (startDateValue.equals(endDateValue) && startTimeValue.isAfter(endTimeValue)) {
            Utilities.customError("Start time and end time are not chronologically plausible!","Please change the time.");
            return null;
        }

        LocalDateTime startDateTimeLocal = LocalDateTime.of(startDateValue, startTimeValue);
        LocalDateTime endDateTimeLocal = LocalDateTime.of(endDateValue, endTimeValue);

        int unusedAppointmentId = AppointmentDAO.getUnusedAppointmentId();

        return new AppointmentModel(unusedAppointmentId, customerSelected, userSelected,
                contactSelected, title.getText(), description.getText(), location.getText(), type.getText(),
                startDateTimeLocal, endDateTimeLocal,
                startDateValue, endDateValue, startTimeValue, endTimeValue);
    }


    /**
     * This method checks if the appointment is within business hours
     * @param appointment the appointment to check
     * @return true if the start and end times of the appointment
     */
    private boolean isAppointmentWithinBusinessHours(AppointmentModel appointment) {
        return Utilities.isWithinBusinessHours(appointment.getStartDateTime())
                && Utilities.isWithinBusinessHours(appointment.getEndDateTime());
    }

    /**
     * Adds an appointment and navigates back to the Appointments Table
     * @param appointment the appointment to add
     * @param actionEvent the event that triggered this method
     * @return true if the appointment
     * @throws SQLException if there is a problem accessing the database
     * @throws IOException if there is a problem navigating back
     */
    private boolean addAppointmentToDBAndNavigateBack(AppointmentModel appointment, ActionEvent actionEvent) throws SQLException, IOException {
        if (AppointmentDAO.addAppointmentToDB(appointment)) {
            Utilities.customInformation("Success!","Appointment successfully added!");
            Stage stage = (Stage) ((Button) actionEvent.getSource()).getScene().getWindow();
            Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
            stage.setScene(new Scene(scene));
            stage.show();
            return true;
        }
        return false;
    }

    /**
     * This method returns to the Appointment View
     * @param event the Cancel button is clicked
     * @throws IOException if there is an Error
     */
    public void returnNavigate(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * This initializes the page or screen
     * @param url url
     * @param resourceBundle resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        title.requestFocus();
        try {
            contact.setItems(ContactDAO.getAllContacts());
            contact.getSelectionModel().selectFirst();
            user.setItems(UserDAO.getAllUsers());
            user.getSelectionModel().selectFirst();
            customer.setItems(CustomerDAO.getAllCustomers());
            customer.getSelectionModel().selectFirst();
            startDate.setValue(LocalDate.now());
            endDate.setValue(LocalDate.now());
            startTime.setItems(FXCollections.observableArrayList(businessHours));
            endTime.setItems(FXCollections.observableArrayList(businessHours));

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
