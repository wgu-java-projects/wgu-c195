package chrislim.c195.controller;

import chrislim.c195.helper.DBLogin;
import chrislim.c195.helper.Utilities;
import chrislim.c195.model.UserModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

import static chrislim.c195.helper.DBLogin.currentUser;

/**
 * Controller for Login View
 * @author Christopher Lim
 */
public class LoginController implements Initializable {
    private final String[] locale = {"English", "French"};

    @FXML private Label titleLabel;
    @FXML private TextField userNameTextField;
    @FXML private PasswordField passwordHidden;
    @FXML private TextField passwordText;
    @FXML private CheckBox showPasswordCheckBox;
    @FXML private Label timeZoneLabel;
    @FXML private Button loginButton;
    @FXML private ChoiceBox<String> languageChoiceBox;

    /**
     * This method is for showing the password that the user type
     * @param event the checkbox for show password
     */
    @FXML
    public void toggleShowPassword(ActionEvent event) {
        if(showPasswordCheckBox.isSelected()) {
            passwordText.setText(passwordHidden.getText());
            passwordText.setVisible(true);
            passwordHidden.setVisible(false);
            return;
        }
        passwordHidden.setText(passwordText.getText());
        passwordHidden.setVisible(true);
        passwordText.setVisible(false);
    }

    /**
     * This initializes the page or screen
     * @param location location
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Sets the current timezone
        // timeZoneLabel.setText(ZoneId.systemDefault().toString());
        timeZoneLabel.setText(Calendar.getInstance().getTimeZone().getID());

        // This is to make sure the toggle for show password is null
        this.toggleShowPassword(null);

        // These items are for configuring locale
        languageChoiceBox.getItems().addAll(locale);
        languageChoiceBox.setValue("English");
        setEnglish();
        languageChoiceBox.setOnAction(this::getLocale);

    }

    /**
     * This method will set the locale to English
     */
    public void setEnglish() {
        Locale.setDefault(new Locale("en"));
        ResourceBundle erb = ResourceBundle.getBundle("/lang_en", Locale.getDefault());
        titleLabel.setText(erb.getString("Title"));
        userNameTextField.setPromptText(erb.getString("Username"));
        passwordText.setPromptText(erb.getString("Password"));
        passwordHidden.setPromptText(erb.getString("Password"));
        showPasswordCheckBox.setText(erb.getString("ShowPassword"));
        loginButton.setText(erb.getString("Login"));
    }

    /**
     * This method will set the Locale to French
     */
    public void setFrench() {
        Locale.setDefault(new Locale("fr"));
        ResourceBundle frb = ResourceBundle.getBundle("/lang_fr", Locale.getDefault());
        titleLabel.setText(frb.getString("Title"));
        userNameTextField.setPromptText(frb.getString("Username"));
        passwordText.setPromptText(frb.getString("Password"));
        passwordHidden.setPromptText(frb.getString("Password"));
        showPasswordCheckBox.setText(frb.getString("ShowPassword"));
        loginButton.setText(frb.getString("Login"));
    }

    /**
     * This method is being used to change the locale between English and French
     * @param event the ChoiceBox is being clicked
     */
    public void getLocale(ActionEvent event) {
        String myLocale = languageChoiceBox.getValue();
        if (myLocale.equals("French")) {
            setFrench();
        }
        else {
            setEnglish();
        }
    }

    /**
     * This method authenticate the User and if the credentials pass
     * it navigates the user to the AppointmentView. If not, the User
     * stays on the LoginView
     * @param event the Login Button is cleared
     * @throws IOException if there is an issue
     */
    public void getLogin(ActionEvent event) throws IOException {
        String username = userNameTextField.getText();
        String password = passwordHidden.getText();
        String password2 = passwordText.getText();

        LocalDateTime now = LocalDateTime.now();
        String fileName = "login_activity.txt";

        FileWriter fileWriter = new FileWriter(fileName, true);
        PrintWriter loginLog = new PrintWriter(fileWriter);

        if (DBLogin.authenticate(username, password)) {
            AppointmentController.setShowAppointmentAlert(true);
            System.out.println(currentUser + "'s login successful!");
            // Define stage and scene objects
            Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
            Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
            stage.setScene(new Scene(scene));
            stage.show();

            //log the successful login
            loginLog.println(username + "'s login was successful at " + now + " (" + ZoneId.systemDefault() + ")");
        } else if (DBLogin.authenticate(username, password2)) {
            AppointmentController.setShowAppointmentAlert(true);
            System.out.println(currentUser + "'s login successful!");
            // Define stage and scene objects
            Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
            Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
            stage.setScene(new Scene(scene));
            stage.show();

            //log the successful login
            loginLog.println(username + "'s login was successful at " + now + " (" + ZoneId.systemDefault() + ")");
        } else {
            System.out.println(currentUser + "'s login unsuccessful!");

            ResourceBundle rb = ResourceBundle.getBundle(
                    Locale.getDefault().getLanguage().equals("fr") ? "/lang_fr" : "/lang_en",
                    Locale.getDefault());

            String errorHeader;
            String errorMessage;

            if (Locale.getDefault().getLanguage().equals("fr")) {
                errorHeader =  rb.getString("errorTitle");
                errorMessage = String.format("%s %s %s/%s %s. %s!", rb.getString("Invalid"), rb.getString("Username"),
                        rb.getString("and"), rb.getString("or"), rb.getString("Password"), rb.getString("Pleasetryagain"));
            } else {
                errorHeader = "Login Error";
                errorMessage = "Invalid Username and/or Password. Please try again!";
            }

            Utilities.customError(errorHeader, errorMessage);


            loginLog.println(username + "'s login was unsuccessful at " + now + " (" + ZoneId.systemDefault() + ")");
        }

        loginLog.close();
    }

    public static UserModel getCurrentUser() {
        return currentUser;
    }
}
