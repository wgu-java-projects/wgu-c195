package chrislim.c195.controller;

import chrislim.c195.DAO.CountryDAO;
import chrislim.c195.DAO.CustomerDAO;
import chrislim.c195.DAO.DivisionDAO;
import chrislim.c195.helper.Utilities;
import chrislim.c195.model.CountryModel;
import chrislim.c195.model.CustomerModel;
import chrislim.c195.model.DivisionModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Controller for Add Customer View
 * @author Christopher Lim
 */
public class AddCustomerController implements Initializable {
    @FXML private TextField customerId;
    @FXML private TextField customerName;
    @FXML private TextField address;
    @FXML private TextField postalCode;
    @FXML private TextField phoneNumber;
    @FXML private ComboBox<CountryModel> country;
    @FXML private ComboBox<DivisionModel> division;
    @FXML private Button addButton;
    @FXML private Button cancelButton;

    /**
     * This method will add a new Customer into the Database
     * @param actionEvent the action event
     * @throws IOException if there is an error
     */
    public void addCustomer(ActionEvent actionEvent) throws IOException {
        boolean customerAdded = false;

        String customerNameText = customerName.getText();
        String addressText = address.getText();
        String postalCodeText = postalCode.getText();
        String phoneNumberText = phoneNumber.getText();

        if (customerNameText == null || customerNameText.isEmpty() ||
                addressText == null || addressText.isEmpty() ||
                postalCodeText == null || postalCodeText.isEmpty() ||
                phoneNumberText == null || phoneNumberText.isEmpty()) {

            Utilities.customError("All fields must be filled!", "Please check to see if there is any missing information.");
        } else {
            String countryName = country.getSelectionModel().getSelectedItem().getCountryName();
            String divisionName = division.getSelectionModel().getSelectedItem().getDivisionName();
            int countryId = country.getSelectionModel().getSelectedItem().getCountryId();
            int divisionId = division.getSelectionModel().getSelectedItem().getDivisionId();
            try {
                int customerId = CustomerDAO.getUnusedCustomerId();

                CustomerModel newCustomer = new CustomerModel(customerId, divisionId, countryId, customerNameText,
                        addressText, postalCodeText, phoneNumberText, countryName, divisionName);

                customerAdded = CustomerDAO.addCustomerToDB(newCustomer);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (customerAdded) {
            Utilities.customInformation("Success!", "Customer successfully added!");
            Stage stage = (Stage)((Button)actionEvent.getSource()).getScene().getWindow();
            Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
            stage.setScene(new Scene(scene));
            stage.show();
        }

    }

    /**
     * This method will update the Division ComboBox depending on the Country the user selected
     * @param actionEvent the action event
     * @throws SQLException if there is an error
     */
    public void updateDivisionComboBox (ActionEvent actionEvent) throws SQLException {
        division.setItems(DivisionDAO.getAllDivisionsByCountryId(country.getSelectionModel().getSelectedItem().getCountryId()));
        division.getSelectionModel().selectFirst();
    }

    /**
     * This method returns to the Appointment View
     * @param event the Cancel button is clicked
     * @throws IOException if there is an Error
     */
    public void returnNavigate(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * Initializes the controller
     * @param url url
     * @param resourceBundle url
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        customerName.requestFocus();
        try {
            country.setItems(CountryDAO.getAllCountries());
            country.getSelectionModel().selectFirst();
            division.setItems(DivisionDAO.getAllDivisionsByCountryId(country.getSelectionModel().getSelectedItem().getCountryId()));
            division.getSelectionModel().selectFirst();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }
}
