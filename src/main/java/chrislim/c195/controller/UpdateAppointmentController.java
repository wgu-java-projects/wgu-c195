package chrislim.c195.controller;

import chrislim.c195.DAO.AppointmentDAO;
import chrislim.c195.DAO.ContactDAO;
import chrislim.c195.DAO.CustomerDAO;
import chrislim.c195.DAO.UserDAO;
import chrislim.c195.helper.Utilities;
import chrislim.c195.model.AppointmentModel;
import chrislim.c195.model.ContactModel;
import chrislim.c195.model.CustomerModel;
import chrislim.c195.model.UserModel;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Stream;

/**
 * Controller for Update Appointment View
 * @author Christopher Lim
 */
public class UpdateAppointmentController implements Initializable {
    private AppointmentModel appointmentToUpdate;
    String businessHours[] =
            { "8:00", "8:30", "9:00", "9:30", "10:00", "10:30", "11:00", "11:30", "13:00", "13:30",
                    "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "18:00", "18:30",
                    "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00"};

    @FXML private TextField appointmentId;
    @FXML private TextField title;
    @FXML private TextField description;
    @FXML private TextField location;
    @FXML private TextField type;
    @FXML private ComboBox<ContactModel> contact;
    @FXML private ComboBox<CustomerModel> customer;
    @FXML private ComboBox<UserModel> user;
    @FXML private DatePicker startDate;
    @FXML private DatePicker endDate;
    @FXML private ComboBox<String> startTime;
    @FXML private ComboBox<String> endTime;
    @FXML private Button updateButton;
    @FXML private Button cancelButton;


    /**
     * This method updates the appointment
     * @param actionEvent the action event
     */
    public void updateAppointment(ActionEvent actionEvent) {
        if (isAnyFieldEmpty()) {
            Utilities.customError("","Please check to see if there is any missing information.");
            return;
        }
        try {
            updateAppointmentFromFormInputs();
            if (isAppointmentWithinBusinessHours(appointmentToUpdate)) {
                if (!AppointmentDAO.checkForOverlap(appointmentToUpdate)) {
                    if (updateAppointmentInDBAndNavigateBack(appointmentToUpdate, actionEvent)) {
                        return;
                    }
                    Utilities.customError("Failed to add appointment!","Check your parameters or debug.");
                } else {
                    Utilities.customError("Overlapping appointments for selected customer!","Please change the time.");
                }
            } else {
                Utilities.customError("Not within business hours!","Please change the time.");
            }
        } catch (SQLException | DateTimeParseException | IOException e) {
            e.printStackTrace();
            Utilities.customError("Incorrect time format!","Please change the time.");
        }
    }

    /**
     * This method checks if any of the required text fields is empty
     * LAMBDA EXPRESSION: Gets a String from the Stream and tests for null or empty.
     * The predicate "(text -> text == null || text.isEmpty())"
     * returns true if the String is null or empty and false otherwise.
     * @return true if any required text box is empty, false otherwise
     */
    private boolean isAnyFieldEmpty() {
        return Stream.of(title.getText(), description.getText(), location.getText(), type.getText())
                .anyMatch(text -> text == null || text.isEmpty());
    }

    /**
     * This method checks if the appointment is within business hours
     * @param appointment the appointment to check
     * @return true if the start and end times of the appointment
     */
    private boolean isAppointmentWithinBusinessHours(AppointmentModel appointment) {
        return Utilities.isWithinBusinessHours(appointment.getStartDateTime())
                && Utilities.isWithinBusinessHours(appointment.getEndDateTime());
    }

    /**
     * This method updates the appointment from the inputs
     * @throws DateTimeParseException if the start or end time text cannot be parsed to a LocalTime
     */
    private void updateAppointmentFromFormInputs() throws DateTimeParseException {
        int contactSelected = contact.getSelectionModel().getSelectedItem().getContactId();
        int customerSelected = customer.getSelectionModel().getSelectedItem().getCustomerId();
        int userSelected = user.getSelectionModel().getSelectedItem().getUserID();
        LocalDate startDateValue = startDate.getValue();
        LocalDate endDateValue = endDate.getValue();
        LocalTime startTimeValue = LocalTime.parse(startTime.getValue());
        LocalTime endTimeValue = LocalTime.parse(endTime.getValue());

        if (startDateValue.isAfter(endDateValue)) {
            Utilities.customError("Start date and end date are not chronologically plausible!","Please change the date.");
            return;
        }

        if (startDateValue.equals(endDateValue) && startTimeValue.isAfter(endTimeValue)) {
            Utilities.customError("Start time and end time are not chronologically plausible!","Please change the time.");
            return;
        }

        LocalDateTime startDateTimeLocal = LocalDateTime.of(startDateValue, startTimeValue);
        LocalDateTime endDateTimeLocal = LocalDateTime.of(endDateValue, endTimeValue);

        appointmentToUpdate.setCustomerId(customerSelected);
        appointmentToUpdate.setUserId(userSelected);
        appointmentToUpdate.setContactId(contactSelected);
        appointmentToUpdate.setTitle(title.getText());
        appointmentToUpdate.setDescription(description.getText());
        appointmentToUpdate.setLocation(location.getText());
        appointmentToUpdate.setType(type.getText());
        appointmentToUpdate.setStartDateTime(startDateTimeLocal);
        appointmentToUpdate.setEndDateTime(endDateTimeLocal);
        appointmentToUpdate.setStartDate(startDateValue);
        appointmentToUpdate.setEndDate(endDateValue);
        appointmentToUpdate.setStartTime(startTimeValue);
        appointmentToUpdate.setEndTime(endTimeValue);
    }

    /**
     * This method updates the appointment and navigates back to AppointmentView
     * @param appointment the appointment
     * @param actionEvent the action event
     * @return true or false
     * @throws SQLException if there is an error
     * @throws IOException if there is an error
     */
    private boolean updateAppointmentInDBAndNavigateBack(AppointmentModel appointment, ActionEvent actionEvent) throws SQLException, IOException {
        if (AppointmentDAO.updateAppointmentInDB(appointment)) {
            Utilities.customInformation("Success!","Appointment successfully updated!");
            Stage stage = (Stage) ((Button) actionEvent.getSource()).getScene().getWindow();
            Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
            stage.setScene(new Scene(scene));
            stage.show();
            return true;
        }
        return false;
    }

    /**
     * This method is used to set the appointment details in the UpdateAppointment view.
     * LAMBDA EXPRESSION: Represented by "c -> c.getContactId() == appointment.getContactId()"
     * Used in this method to conveniently and concisely filter the stream of items in the ComboBoxes.
     * Allows us to directly express what we are looking for without having to write explicit loops.
     * @param appointment The Appointment object to be updated.
     */
    public void setAppointmentToUpdate(AppointmentModel appointment) {
        this.appointmentToUpdate = appointment;
        appointmentId.setText(Integer.toString(appointment.getAppointmentId()));
        title.setText(appointment.getTitle());
        description.setText(appointment.getDescription());
        location.setText(appointment.getLocation());
        type.setText(appointment.getType());

        ContactModel contactToSelect = contact.getItems().stream()
                .filter(c -> c.getContactId() == appointment.getContactId())
                .findFirst()
                .orElse(null);
        contact.getSelectionModel().select(contactToSelect);

        CustomerModel customerToSelect = customer.getItems().stream()
                .filter(c -> c.getCustomerId() == appointment.getCustomerId())
                .findFirst()
                .orElse(null);
        customer.getSelectionModel().select(customerToSelect);

        UserModel userToSelect = user.getItems().stream()
                .filter(u -> u.getUserID() == appointment.getUserId())
                .findFirst()
                .orElse(null);
        user.getSelectionModel().select(userToSelect);

        startDate.setValue(appointment.getStartDate());
        endDate.setValue(appointment.getEndDate());
        startTime.setValue(appointment.getStartTime().toString());
        endTime.setValue(appointment.getEndTime().toString());
    }

    /**
     * This method returns to the Appointment View
     * @param event the Cancel button is clicked
     * @throws IOException if there is an Error
     */
    public void returnNavigate(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * This initializes the page or screen
     * @param url url
     * @param resourceBundle rb
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        title.requestFocus();
        try {
            contact.setItems(ContactDAO.getAllContacts());
            contact.getSelectionModel().selectFirst();
            user.setItems(UserDAO.getAllUsers());
            user.getSelectionModel().selectFirst();
            customer.setItems(CustomerDAO.getAllCustomers());
            customer.getSelectionModel().selectFirst();
            startDate.setValue(LocalDate.now());
            endDate.setValue(LocalDate.now());
            startTime.setItems(FXCollections.observableArrayList(businessHours));
            endTime.setItems(FXCollections.observableArrayList(businessHours));
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
