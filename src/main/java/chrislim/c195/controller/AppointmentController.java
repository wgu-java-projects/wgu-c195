package chrislim.c195.controller;

import chrislim.c195.DAO.*;
import chrislim.c195.helper.DBLogin;
import chrislim.c195.helper.Utilities;
import chrislim.c195.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Controller for Appointment View
 * @author Christopher Lim
 */
public class AppointmentController implements Initializable {
    private static boolean showAppointmentAlert = false;

    // Appointment tab
    @FXML private TableView<AppointmentModel> appointmentTableView;
    @FXML private TableColumn<AppointmentModel, Integer> appointmentId;
    @FXML private TableColumn<AppointmentModel, String> title;
    @FXML private TableColumn<AppointmentModel, String> description;
    @FXML private TableColumn<AppointmentModel, String> location;
    @FXML private TableColumn<AppointmentModel, String> type;
    @FXML private TableColumn<AppointmentModel, Integer> contact;
    @FXML private TableColumn<AppointmentModel, LocalTime> startTime;
    @FXML private TableColumn<AppointmentModel, LocalTime> endTime;
    @FXML private TableColumn<AppointmentModel, LocalDate> startDate;
    @FXML private TableColumn<AppointmentModel, LocalDate> endDate;
    @FXML private TableColumn<AppointmentModel, Integer> userId;
    @FXML private TableColumn<AppointmentModel, Integer> appointmentCustomerId;
    @FXML private RadioButton viewByWeekRadioButton;
    @FXML private RadioButton viewByMonthRadioButton;
    @FXML private RadioButton viewAllRadioButton;
    @FXML private DatePicker appointmentSearchDatePicker;

    // Customer tab
    @FXML private TableView<CustomerModel> customerTableView;
    @FXML private TableColumn<CustomerModel, Integer> customerId;
    @FXML private TableColumn<CustomerModel, String> name;
    @FXML private TableColumn<CustomerModel, String> address;
    @FXML private TableColumn<CustomerModel, String> postalCode;
    @FXML private TableColumn<CustomerModel, String> phone;
    @FXML private TableColumn<CustomerModel, String> country;
    @FXML private TableColumn<CustomerModel, String> state;
    @FXML private TextField customerSearchField;

    // Report by Contact
    @FXML private Tab contactReportTab;
    @FXML private ComboBox<ContactModel> contactComboBox;
    @FXML private TableView<AppointmentModel> getContactTableView;
    @FXML private TableColumn<AppointmentModel, Integer> apptIDColumn;
    @FXML private TableColumn<AppointmentModel, String> titleColumn;
    @FXML private TableColumn<AppointmentModel, String> descriptionColumn;
    @FXML private TableColumn<AppointmentModel, String> locationColumn;
    @FXML private TableColumn<AppointmentModel, String> typeColumn;
    @FXML private TableColumn<AppointmentModel, LocalDate> startDateColumn;
    @FXML private TableColumn<AppointmentModel, LocalDate> endDateColumn;
    @FXML private TableColumn<AppointmentModel, LocalTime> startTimeColumn;
    @FXML private TableColumn<AppointmentModel, LocalTime> endTimeColumn;
    @FXML private TableColumn<AppointmentModel, Integer> customerIDColumn;
    @FXML private Label totalAppointmentsLabel;

    // Report by Country
    @FXML private Tab countryReportTab;
    @FXML private ComboBox<CountryModel> countryComboBox;
    @FXML private TableView<CustomerModel> getCustomerTableView;
    @FXML private TableColumn<CustomerModel, Integer> customerIDCountryReport;
    @FXML private TableColumn<CustomerModel, String> customerNameCountryReport;
    @FXML private TableColumn<CustomerModel, String> addressCountryReport;
    @FXML private TableColumn<CustomerModel, String> postalCodeCountryReport;
    @FXML private TableColumn<CustomerModel, String> phoneCountryReport;
    @FXML private TableColumn<CustomerModel, String> stateCountryReport;
    @FXML private Label totalCustomersLabel;

    // Report by Month
    @FXML private Tab monthReportTab;
    @FXML private TableView<ReportModel> getMonthTableView;
    @FXML private TableColumn<ReportModel, String> typeMonthColumn;
    @FXML private TableColumn<ReportModel, String> monthColumn;
    @FXML private TableColumn<ReportModel, Integer> countColumn;
    @FXML private TextField monthSearchField;
    @FXML private Label totalMonthsLabel;

    /**
     * This method logs out the User
     * @param event the logout button is clicked
     */
    public void logOut(ActionEvent event) throws IOException {
        Optional<ButtonType> result = Utilities.customConfirmation("Logout?","Do you really want to logout?");
        if (result.isPresent() && result.get() == ButtonType.OK) {
            System.out.println("Logging out.");
            Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
            Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/LoginView.fxml")));
            stage.setScene(new Scene(scene));
            stage.show();
        } else {
            System.out.println("Didn't logout.");
        }
    }

    // Appointment tab
    /**
     * This method navigates the user to the Add Appointment View
     * @param event the Add Appointment Button is clicked
     * @throws IOException if there is an error
     */
    public void addAppointmentNavigate(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AddAppointmentView.fxml")));
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * This method navigates the user to the Update Appointment View
     * @param event the Update Appointment Button is clicked
     * @throws IOException if there is an error
     */
    public void updateAppointmentNavigate(ActionEvent event) throws IOException {
        if(appointmentTableView.getSelectionModel().isEmpty()) {
            Utilities.customError("No appointment selected!","Please select an appointment first to affect any change!");
        }
        AppointmentModel selectedAppointment = appointmentTableView.getSelectionModel().getSelectedItem();
        if (selectedAppointment == null) {
            System.out.println("Selected appointment null.");
            return;
        }

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/chrislim/c195/view/UpdateAppointmentView.fxml"));
        loader.load();
        UpdateAppointmentController UAC = loader.getController();
        UAC.setAppointmentToUpdate(selectedAppointment);
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        Parent scene = loader.getRoot();
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * This method deletes the appointment from the Appointment View Database
     * @param actionEvent the action event
     * @throws SQLException if there is an error
     */
    public void deleteAppointment(ActionEvent actionEvent) throws SQLException {
        if(appointmentTableView.getSelectionModel().isEmpty()) {
            Utilities.customError("No appointment selected!","Please select an appointment first to affect any change!");
        }
        else {
            int appointmentID = appointmentTableView.getSelectionModel().getSelectedItem().getAppointmentId();
            String appointmentType = appointmentTableView.getSelectionModel().getSelectedItem().getType();


            Optional<ButtonType> result = Utilities.customConfirmation("Delete appointment?","Do you really want to delete the appointment with ID: " + appointmentID +  " and TYPE: " + appointmentType + " from the database?");

            if (result.isPresent() && result.get() == ButtonType.OK) {

                int selectedAppointmentId = appointmentTableView.getSelectionModel().getSelectedItem().getAppointmentId();
                try {
                    if (AppointmentDAO.deleteAppointmentFromDB(selectedAppointmentId)) {
                        System.out.println("Deletion successful!");
                        Utilities.customInformation("Appointment deleted!","The appointment with ID: " + appointmentID +  " and TYPE: " + appointmentType + " has been deleted.");
                    } else {
                        System.out.println("Something mysterious has happened and the appointment wasn't deleted!");
                    }
                    appointmentTableView.setItems(AppointmentDAO.getAllAppointments());
                } catch (SQLException e) {
                    System.out.println("There was an error deleting the appointment.");
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * This method sets the value of true or false for
     * the appointment alert
     * @param value true or false
     */
    public static void setShowAppointmentAlert(boolean value) {
        showAppointmentAlert = value;
    }

    /**
     * This method uses for the view this week radio button
     * @param actionEvent the action event
     */
    public void viewThisWeek(ActionEvent actionEvent) {
        ObservableList<AppointmentModel> appointments = AppointmentDAO.getAllAppointmentsThisWeek();
        appointmentTableView.setItems(appointments);
    }

    /**
     * This method uses for the view this month radio button
     * @param actionEvent the action event
     */
    public void viewThisMonth(ActionEvent actionEvent) {
        ObservableList<AppointmentModel> appointments = AppointmentDAO.getAllAppointmentsThisMonth();
        appointmentTableView.setItems(appointments);
    }

    /**
     * This method uses to view all the appointments
     * @param actionEvent the action event
     */
    public void viewAll(ActionEvent actionEvent) {
        ObservableList<AppointmentModel> appointments = AppointmentDAO.getAllAppointments();
        appointmentTableView.setItems(appointments);
    }

    /**
     * This method uses the search by date to find an appointment
     * @param actionEvent the action event
     */
    public void searchByDate(ActionEvent actionEvent) {
        LocalDate selectedDate = appointmentSearchDatePicker.getValue();
        if (selectedDate == null) {
            System.out.println("No date selected!");
            return;
        }

        ObservableList<AppointmentModel> allAppointments = AppointmentDAO.getAllAppointments();

        ObservableList<AppointmentModel> filteredAppointments = allAppointments.stream()
                .filter(appointment -> selectedDate.equals(appointment.getStartDateTime().toLocalDate()))
                .collect(Collectors.toCollection(FXCollections::observableArrayList));

        appointmentTableView.setItems(filteredAppointments);
    }

    // Customer tab
    /**
     * This method navigates the user to the Add Customer View
     * @param event the Add Customer Button is clicked
     * @throws IOException if there is an error
     */
    public void addCustomerNavigate(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AddCustomerView.fxml")));
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * This method navigates the user to the Update Customer View
     * @param event the Update Customer Button is clicked
     * @throws IOException if there is an error
     */
    public void updateCustomerNavigate(ActionEvent event) throws IOException {
        if(customerTableView.getSelectionModel().isEmpty()) {
            Utilities.customError("No customer selected!", "Please select a customer first to affect any change!");
        } else {
            CustomerModel selectedCustomer = customerTableView.getSelectionModel().getSelectedItem();
            if (selectedCustomer == null) {
                System.out.println("Selected customer is null.");
                return;
            }
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/chrislim/c195/view/UpdateCustomerView.fxml"));
            loader.load();

            UpdateCustomerController UCC = loader.getController();
            UCC.setCustomerToUpdate(selectedCustomer);

            Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
            Parent scene = loader.getRoot();
            stage.setScene(new Scene(scene));
            stage.show();
        }

    }

    public void deleteCustomer(ActionEvent actionEvent) {
        if(customerTableView.getSelectionModel().isEmpty()) {
            Utilities.customError("No customer selected!", "Please select a customer first to affect any change!");
        } else {
            CustomerModel selectedCustomer = customerTableView.getSelectionModel().getSelectedItem();
            // Check if the selected customer has any associated appointments
            ObservableList<AppointmentModel> allAppointments = AppointmentDAO.getAllAppointments();
            ObservableList<AppointmentModel> associatedAppointments = FXCollections.observableArrayList(
                    allAppointments.stream()
                            .filter(appointment -> appointment.getCustomerId() == selectedCustomer.getCustomerId())
                            .collect(Collectors.toList())
            );

            int customerId = selectedCustomer.getCustomerId();
            String customerName = selectedCustomer.getCustomerName();

            Optional<ButtonType> result =  Utilities.customConfirmation("Delete customer?", "Do you really want to delete the customer with ID: " + customerId + " and NAME: " + customerName + " and all associated appointments from the database?");

            if (result.isPresent() && result.get() == ButtonType.OK) {
                try {
                    for (AppointmentModel appointment : associatedAppointments) {
                        AppointmentDAO.deleteAppointmentFromDB(appointment.getAppointmentId());
                    }
                    if (CustomerDAO.deleteCustomerFromDB(customerId)) {
                        System.out.println("Deletion successful!");

                        Utilities.customInformation("Customer and associated appointments deleted!","The customer with ID: " + customerId + " and NAME: " + customerName + " and all associated appointments have been deleted.");
                        appointmentTableView.setItems(AppointmentDAO.getAllAppointments());
                    } else {
                        System.out.println("Something mysterious has happened and the customer wasn't deleted!");
                    }
                    customerTableView.setItems(CustomerDAO.getAllCustomers());
                } catch (SQLException e) {
                    System.out.println("There was an error deleting the customer and/or the associated appointments.");
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method search for the Customer name in the database.
     * LAMBDA EXPRESSION: The expression used in this method is
     * "(customer -> customer.getCustomerName().toLowerCase().contains(searchText.toLowerCase()))".
     * This predicate function examines if a Customer object's lower-case name contains the search text.
     * It returns true if the customer's name contains the search phrase, false otherwise.
     */
    public void searchCustomer() {
        String searchText = customerSearchField.getText();

        if (searchText == null || searchText.isEmpty()) {
            customerTableView.setItems(CustomerDAO.getAllCustomers());
            return;
        }

        ObservableList<CustomerModel> allCustomers = CustomerDAO.getAllCustomers();

        ObservableList<CustomerModel> filteredCustomers = allCustomers.stream()
                .filter(customer -> customer.getCustomerName().toLowerCase().contains(searchText.toLowerCase()))
                .collect(Collectors.toCollection(FXCollections::observableArrayList));

        customerTableView.setItems(filteredCustomers);
    }

    /**
     * This method updates the Report Table by Contact
     * @param actionEvent the action event
     */
    public void updateTableBasedOnContact(ActionEvent actionEvent) {
        int selectedContactId = contactComboBox.getSelectionModel().getSelectedItem().getContactId();
        if (selectedContactId != 0) {
            ObservableList<AppointmentModel> appointmentsOfSelectedContact =
                    AppointmentDAO.getAppointmentsByContact(selectedContactId);
            getContactTableView.setItems(appointmentsOfSelectedContact);
            int totalAppointments = getContactTableView.getItems().size();
            totalAppointmentsLabel.setText("Total Appointments: " + totalAppointments);
        }
    }

    /**
     * This method will update the table based on country
     * @param actionEvent the action event
     */
    public void updateTableBasedOnCountry(ActionEvent actionEvent) {
        int selectedCountryId = countryComboBox.getSelectionModel().getSelectedItem().getCountryId();

        if (selectedCountryId != 0) {
            ObservableList<CustomerModel> customersInSelectedCountry =
                    CustomerDAO.getCustomersByCountry(selectedCountryId);

            getCustomerTableView.setItems(customersInSelectedCountry);
            int totalCustomers = getCustomerTableView.getItems().size();
            totalCustomersLabel.setText("Total Customers: " + totalCustomers);
        }
    }

    /**
     * This method searches for months in the report tab
     * LAMBDA EXPRESSION: The expression used in this method is
     * "(month -> month.getMonth().toLowerCase().contains(searchText.toLowerCase()))".
     * This predicate function examines if a Months object's lower-case name contains the search text.
     * It returns true if the customer's name contains the search phrase, false otherwise.
     * @throws SQLException if there is an error
     */
    public void searchMonth() throws SQLException {
        String searchText = monthSearchField.getText();

        if (searchText == null || searchText.isEmpty()) {
            getMonthTableView.setItems(ReportDAO.getAllReports());
            return;
        }

        ObservableList<ReportModel> allMonths = ReportDAO.getAllReports();

        ObservableList<ReportModel> filteredMonths = allMonths.stream()
                .filter(month -> month.getMonth().toLowerCase().contains(searchText.toLowerCase()))
                .collect(Collectors.toCollection(FXCollections::observableArrayList));

        getMonthTableView.setItems(filteredMonths);
        int totalMonths = getMonthTableView.getItems().size();
        totalMonthsLabel.setText("Total Months: " + totalMonths);
    }


    /**
     * This initializes the page or screen
     * @param url url
     * @param resourceBundle resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Make a toggle group for radio button and make view all as default
        ToggleGroup viewToggleGroup = new ToggleGroup();
        this.viewByWeekRadioButton.setToggleGroup(viewToggleGroup);
        this.viewByMonthRadioButton.setToggleGroup(viewToggleGroup);
        this.viewAllRadioButton.setToggleGroup(viewToggleGroup);
        this.viewAllRadioButton.setSelected(true);

        // Sets the cell value for appointment
        appointmentId.setCellValueFactory(new PropertyValueFactory<>("appointmentId"));
        title.setCellValueFactory(new PropertyValueFactory<>("title"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        location.setCellValueFactory(new PropertyValueFactory<>("location"));
        type.setCellValueFactory(new PropertyValueFactory<>("type"));
        contact.setCellValueFactory(new PropertyValueFactory<>("contactId"));
        startTime.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        endTime.setCellValueFactory(new PropertyValueFactory<>("endTime"));
        startDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        endDate.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        userId.setCellValueFactory(new PropertyValueFactory<>("userId"));
        appointmentCustomerId.setCellValueFactory(new PropertyValueFactory<>("customerId"));

        ObservableList<AppointmentModel> appointments = AppointmentDAO.getAllAppointments();
        appointmentTableView.setItems(appointments);

        // Sets the cell value for customer
        customerId.setCellValueFactory(new PropertyValueFactory<>("customerId"));
        name.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        address.setCellValueFactory(new PropertyValueFactory<>("address"));
        postalCode.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
        country.setCellValueFactory(new PropertyValueFactory<>("countryName"));
        state.setCellValueFactory(new PropertyValueFactory<>("divisionName"));

        ObservableList<CustomerModel> customers = CustomerDAO.getAllCustomers();
        customerTableView.setItems(customers);

        AppointmentModel upcomingAppointment = null;
        try {
            upcomingAppointment = AppointmentDAO.getUpcomingAppointment(DBLogin.getCurrentUser().getUserID());
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        // Check if the appointment falls within the next 15 minutes and business hours
        if (upcomingAppointment != null) {
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime in15Minutes = now.plusMinutes(15);

            if (now.isBefore(upcomingAppointment.getStartDateTime()) && in15Minutes.isAfter(upcomingAppointment.getStartDateTime())
                    && Utilities.isWithinBusinessHours(upcomingAppointment.getStartDateTime())) {
                Utilities.customInformation("You have an appointment starting soon within 15 minutes!", "Appointment ID: " + upcomingAppointment.getAppointmentId()
                        + "\nDate: " + upcomingAppointment.getStartDate()
                        + "\nTime: " + upcomingAppointment.getStartTime());
            }
        } else {
            if (showAppointmentAlert) {
                Utilities.customInformation("No Upcoming Appointments", "You have no appointments starting within the next 15 minutes.");
                showAppointmentAlert = false;
            }
        }

        try {


            ObservableList<ContactModel> allContacts = ContactDAO.getAllContacts();
            contactComboBox.setItems(allContacts);

            ObservableList<AppointmentModel> appointmentsContacts = AppointmentDAO.getAllAppointments();
            getContactTableView.setItems(appointmentsContacts);

            ObservableList<CountryModel> allCountries = CountryDAO.getAllCountries();
            countryComboBox.setItems(allCountries);

            ObservableList<CustomerModel> customersCountry = CustomerDAO.getAllCustomers();
            getCustomerTableView.setItems(customersCountry);

            ObservableList<ReportModel> monthReports = ReportDAO.getAllReports();
            getMonthTableView.setItems(monthReports);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Sets the cell value for contact
        apptIDColumn.setCellValueFactory(new PropertyValueFactory<>("appointmentId"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        locationColumn.setCellValueFactory(new PropertyValueFactory<>("location"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        startDateColumn.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        endDateColumn.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        startTimeColumn.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        endTimeColumn.setCellValueFactory(new PropertyValueFactory<>("endTime"));
        customerIDColumn.setCellValueFactory(new PropertyValueFactory<>("customerId"));

        // Sets the cell value for country
        customerIDCountryReport.setCellValueFactory(new PropertyValueFactory<CustomerModel, Integer>("customerId"));
        customerNameCountryReport.setCellValueFactory(new PropertyValueFactory<CustomerModel, String>("customerName"));
        addressCountryReport.setCellValueFactory(new PropertyValueFactory<CustomerModel, String>("address"));
        postalCodeCountryReport.setCellValueFactory(new PropertyValueFactory<CustomerModel, String>("postalCode"));
        phoneCountryReport.setCellValueFactory(new PropertyValueFactory<CustomerModel, String>("phone"));
        stateCountryReport.setCellValueFactory(new PropertyValueFactory<CustomerModel, String>("divisionName"));

        // Sets the cell value for month
        monthColumn.setCellValueFactory(new PropertyValueFactory<ReportModel, String>("month"));
        typeMonthColumn.setCellValueFactory(new PropertyValueFactory<ReportModel, String>("type"));
        countColumn.setCellValueFactory(new PropertyValueFactory<ReportModel, Integer>("count"));
    }
}
