package chrislim.c195.controller;

import chrislim.c195.DAO.CountryDAO;
import chrislim.c195.DAO.CustomerDAO;
import chrislim.c195.DAO.DivisionDAO;
import chrislim.c195.helper.Utilities;

import chrislim.c195.model.CountryModel;
import chrislim.c195.model.CustomerModel;

import chrislim.c195.model.DivisionModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Stream;

/**
 * Controller for Update Customer View
 * @author Christopher Lim
 */
public class UpdateCustomerController implements Initializable {
    private CustomerModel customerToUpdate;

    @FXML private TextField customerId;
    @FXML private TextField customerName;
    @FXML private TextField address;
    @FXML private TextField postalCode;
    @FXML private TextField phoneNumber;
    @FXML private ComboBox<CountryModel> country;
    @FXML private ComboBox<DivisionModel> division;
    @FXML private Button updateButton;
    @FXML private Button cancelButton;

    /**
     * This method will update thc Customer Data in the database
     * @param actionEvent the action event
     * @throws IOException the action event
     */
    public void updateCustomer(ActionEvent actionEvent) throws IOException {
        if (isAnyFieldEmpty()) {
            Utilities.customError("All fields must be filled!","Please check to see if there is any missing information.");
            return;
        }
        try {
            updateCustomerFromFormInputs();
            if (updateCustomerInDBAndNavigateBack(customerToUpdate, actionEvent)) {
                return;
            }
            System.out.println("Failed to update customer!");
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            Utilities.customError("Incorrect time format!", "Please change the time.");
        }
    }

    /**
     * This method sets thc Customer to be updated
     * @param customer the customer
     */
    public void setCustomerToUpdate(CustomerModel customer) {
        this.customerToUpdate = customer;
        customerId.setText(Integer.toString(customer.getCustomerId()));
        customerName.setText(customer.getCustomerName());
        address.setText(customer.getAddress());
        postalCode.setText(customer.getPostalCode());
        phoneNumber.setText(customer.getPhone());

        CountryModel countryToSelect = country.getItems().stream()
                .filter(c -> c.getCountryId() == customer.getCountryId())
                .findFirst()
                .orElse(null);
        country.getSelectionModel().select(countryToSelect);

        try {
            division.setItems(DivisionDAO.getAllDivisionsByCountryId(country.getSelectionModel().getSelectedItem().getCountryId()));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        DivisionModel divisionToSelect = division.getItems().stream()
                .filter(d -> d.getDivisionId() == customer.getDivisionId())
                .findFirst()
                .orElse(null);
        division.getSelectionModel().select(divisionToSelect);
    }

    /**
     * This method will updates the Customer from the input fields
     */
    private void updateCustomerFromFormInputs() {
        String customerNameText = customerName.getText();
        String addressText = address.getText();
        String postalCodeText = postalCode.getText();
        String phoneNumberText = phoneNumber.getText();
        int countryId = country.getSelectionModel().getSelectedItem().getCountryId();
        int divisionId = division.getSelectionModel().getSelectedItem().getDivisionId();

        customerToUpdate.setCustomerName(customerNameText);
        customerToUpdate.setAddress(addressText);
        customerToUpdate.setPostalCode(postalCodeText);
        customerToUpdate.setPhone(phoneNumberText);
        customerToUpdate.setCountryId(countryId);
        customerToUpdate.setDivisionId(divisionId);
    }

    /**
     * This method updates the Customer Data and return back to the AppointmentView
     * @param customer the customer
     * @param event the event
     * @return true or false
     * @throws SQLException if there is an error
     * @throws IOException if there is an error
     */
    private boolean updateCustomerInDBAndNavigateBack(CustomerModel customer, ActionEvent event) throws SQLException, IOException {
        if (CustomerDAO.updateCustomerInDB(customer)) {
            Utilities.customInformation("Success!","Customer successfully updated!");
            Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
            Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
            stage.setScene(new Scene(scene));
            stage.show();
            return true;
        }
        return false;
    }

    /**
     * This method checks if any of the required text fields is empty
     * LAMBDA EXPRESSION: Gets a String from the Stream and tests for null or empty.
     * The predicate "(text -> text == null || text.isEmpty())"
     * returns true if the String is null or empty and false otherwise.
     * @return true if any required text box is empty, false otherwise
     */
    private boolean isAnyFieldEmpty() {
        return Stream.of(customerName.getText(), address.getText(), postalCode.getText(), phoneNumber.getText())
                .anyMatch(text -> text == null || text.isEmpty());
    }

    /**
     * This method will updates the division ComboBox if the Country gets change
     * @param event the event
     * @throws SQLException if there is any error
     */
    public void updateDivisionComboBox (ActionEvent event) throws SQLException {
        division.setItems(DivisionDAO.getAllDivisionsByCountryId(country.getSelectionModel().getSelectedItem().getCountryId()));
        division.getSelectionModel().selectFirst();
    }

    /**
     * This method returns to the Appointment View
     * @param event the Cancel button is clicked
     * @throws IOException if there is an Error
     */
    public void returnNavigate(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        Parent scene = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/chrislim/c195/view/AppointmentView.fxml")));
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * Initializes the controller
     * @param url url
     * @param resourceBundle resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        customerName.requestFocus();
        try {
            country.setItems(CountryDAO.getAllCountries());
            country.getSelectionModel().selectFirst();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }
}
