package chrislim.c195.helper;

import chrislim.c195.model.UserModel;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static chrislim.c195.helper.JDBC.connection;

/**
 * A DBLogin class that handles the database operations for login
 * @author Christopher Lim
 */
public class DBLogin {
    public static UserModel currentUser;

    /**
     * This method execute an SQL query to authenticate a user.
     * @param userName the username entered by the user
     * @param password the password entered by the password
     * @return optional user object if authentication is successful
     */
    public static Optional<UserModel> loginQuery(String userName, String password) {
        String sql = "SELECT * FROM users WHERE User_Name = ? AND Password = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userName);
            statement.setString(2, password);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(createUserFromResultSet(resultSet));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * This method creates a user object from the provided result set.
     * @param resultSet the result set from the SQL query
     * @return userModel with the data from the result
     * @throws SQLException checks for error
     */
    private static UserModel createUserFromResultSet(ResultSet resultSet) throws SQLException {
        int userId = resultSet.getInt("User_ID");
        String foundUserName = resultSet.getString("User_Name");
        String foundPassword = resultSet.getString("Password");
        return new UserModel(userId, foundUserName, foundPassword);
    }

    /**
     * This method returns the current user
     * @return the currentUser
     */
    public static UserModel getCurrentUser() {
        return currentUser;
    }

    /**
     * This method authenticate if the user exist in the database
     * @param username the username
     * @param password the password
     * @return true or false
     * @throws IOException if there is an error
     */
    public static boolean authenticate(String username, String password) throws IOException {
        Optional<UserModel> userResult = loginQuery(username, password);
        if (userResult.isPresent()) {
            currentUser = userResult.get();
            return true;
        } else {
            return false;
        }
    }
}
