package chrislim.c195.helper;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * A Utility class for time zone conversion and
 * different types of alerts
 * @author Christopher Lim
 */
public abstract class Utilities {
    private static final ZoneId EASTERN_TIME = ZoneId.of("America/New_York");
    private static final LocalTime START_TIME = LocalTime.of(8, 0);
    private static final LocalTime END_TIME = LocalTime.of(22, 0);

    /**
     * This method converts the locale time to EST
     * @param dateTime the time to convert
     * @return converted the zoned date time
     */
    public static ZonedDateTime convertToEasternTime(LocalDateTime dateTime) {
        return dateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(EASTERN_TIME);
    }

    /**
     * This method checks if the time selected is within
     * the business start time and end time
     * @param localDateTime this checks the time
     * @return either true or false
     */
    public static boolean isWithinBusinessHours(LocalDateTime localDateTime) {
        ZonedDateTime easternTime = convertToEasternTime(localDateTime);
        LocalTime time = easternTime.toLocalTime();
        return !time.isBefore(START_TIME) && !time.isAfter(END_TIME);
    }

    /**
     * This method shows an alert window for empty field
     * @param fieldName the field name
     */
    public static void emptyAlert(String fieldName){
        Alert invalidData = new Alert(Alert.AlertType.ERROR,
                fieldName + " has no data entered");

        Optional<ButtonType> alert = invalidData.showAndWait();
        return;
    }

    /**
     * This method shows an alert window for custom error message
     * @param headerText the header text
     * @param errorMessage the error message
     */
    public static void customError(String headerText, String errorMessage){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(headerText);
        alert.setContentText(errorMessage);
        alert.showAndWait();
    }

    /**
     * This method shows an alert window for custom information message
     * @param headerText the header text
     * @param alertMessage the alert message
     */
    public static void customInformation(String headerText, String alertMessage){
        Alert alert = new Alert(Alert.AlertType.INFORMATION,
                alertMessage);
        alert.setTitle("Information");
        alert.setHeaderText(headerText);
        alert.setContentText(alertMessage);
        alert.showAndWait();
    }


    /**
     * This method shows an alert for confirmation message
     * @param headerText the header text
     * @param alertMessage the alert message
     * @return the Optional empty
     */
    public static Optional<ButtonType> customConfirmation(String headerText, String alertMessage){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        alert.setTitle("Confirmation");
        alert.setHeaderText(headerText);
        alert.setContentText(alertMessage);
       return alert.showAndWait();

    }

}
