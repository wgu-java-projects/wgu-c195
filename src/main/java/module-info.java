module chrislim.c195 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.j;
    requires atlantafx.base;


    opens chrislim.c195.controller to javafx.fxml;
    exports chrislim.c195.controller;
    exports chrislim.c195.DAO;
    exports chrislim.c195.helper;
    exports chrislim.c195.model;
    exports chrislim.c195.main;
}