# Appointment Scheduler

### Purpose:
This application's goal is to let users arrange appointments between clients.
In addition to recording successful and unsuccessful login attempts in a log file, the application checks login credentials.
From the appointment view, the user can add, edit, and remove customers and appointments.
On the reports page, the user can examine reports that contain condensed information.

### Project Information:
```
Author: Christopher Lim
Contact: clim18@my.wgu.edu
Version: 1.0
Date: 11/20/2023

IDE: IntelliJ 2023.2.5 (Community Edition)
JDK: 17.0.9 2023-10-17 LTS
JavaFX: 17.0.6
MySQL Connector: 8.2.0
```

### Instructions:
- Open the project in IntelliJ IDEA.
- Make sure JDK 17.0.9 and JavaFX 17.0. 6 are properly configured.
- Make sure mysql-connector-java-8.2.0 is included in the library.
- Run the Main class to start the application.
- Log in with username "test", password "test".

### Additional Report:
The total number of consumers in each country is displayed to the user in the extra report I added to my report page.
It just lists every country that has a customer in it together with the overall number of customers in that country.

### Lambda Expression Location:
- AddAppointmentController.isAnyFieldEmpty
- AppointmentController.searchCustomer
- AppointmentController.searchMonth
- UpdateAppointmentController.isAnyFieldEmpty
- UpdateAppointmentController.setAppointmentToUpdate
- UpdateCustomerController.isAnyFieldEmpty

### Additional Resources:
- [C195 Students Start Here](https://protect-us.mimecast.com/s/T6wIC82zR6twz6K3wfnw7n6?domain=srm--c.na127.visual.force.com)
- [C195 Check-Off List](https://protect-us.mimecast.com/s/syj7C9rAVQfYMkpnYSEgMOP?domain=srm--c.na127.visual.force.com)
- [C195 Virtual Environment Information](https://protect-us.mimecast.com/s/CDTsC0RoJVCrMGLvrf2lNdb?domain=srm--c.na127.visual.force.com)
- [Tutorials Point MySQL](https://www.tutorialspoint.com/mysql/index.htm)
- [AtlantaFX for JavaFX CSS Theme](https://mkpaz.github.io/atlantafx/)
